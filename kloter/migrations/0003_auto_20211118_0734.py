# Generated by Django 2.2.6 on 2021-11-18 00:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kloter', '0002_auto_20211118_0721'),
    ]

    operations = [
        migrations.RenameField(
            model_name='numberkloter',
            old_name='putaran',
            new_name='ronde',
        ),
        migrations.RenameField(
            model_name='person',
            old_name='putaran',
            new_name='ronde',
        ),
    ]
